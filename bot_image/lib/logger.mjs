let scope = "";

export function log(...msgs) {
  console.log(...msgs);
}

export function warn(...msgs) {
  console.warn(...msgs);
}
export function setScope(name) {
  if (scope !== "") {
    console.groupEnd();
  }
  scope = name;
  if (scope !== "") {
    console.group(scope);
  }
}
