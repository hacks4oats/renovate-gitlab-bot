const {
  createServerConfig,
  baseConfig,
  defaultLabels,
  availableRouletteReviewerByRole,
} = require("../lib/shared");
const { updateDangerReviewComponent } = require("../lib/components");

const enableWithBumpStrategy = {
  rangeStrategy: "bump",
  enabled: true,
};

module.exports = createServerConfig([
  {
    repository: "gitlab-renovate-forks/gitlab-eclipse-plugin",
    ...baseConfig,
    includePaths: ["*", "gitlab-language-server/*"],
    labels: [
      ...defaultLabels,
      "group::editor extensions",
      "devops::create",
      "section::dev",
      "type::maintenance",
    ],
    semanticCommits: "disabled",
    prConcurrentLimit: 2,
    reviewers: availableRouletteReviewerByRole("gitlab-eclipse-plugin"),
    reviewersSampleSize: 1,
    enabledManagers: ["npm", "gradle", "custom.regex"],
    packageRules: [
      {
        ...enableWithBumpStrategy,
        matchPackagePrefixes: [
          "io.gitlab.arturbosch.detekt",
          "org.jetbrains.kotlin",
        ],
        excludePackagePrefixes: ["org.jetbrains.kotlinx"],
        groupName: "Kotlin",
      },
      {
        ...enableWithBumpStrategy,
        matchPackagePrefixes: [
          "io.kotest",
          "io.mockk",
        ],
        groupName: "Testing",
      },
      {
        ...enableWithBumpStrategy,
        matchPackagePrefixes: [
          "org.eclipse.platform",
        ],
        groupName: "Eclipse Platform",
      },
      {
        ...enableWithBumpStrategy,
        matchPackagePrefixes: [
          "dev.equo",
        ],
        groupName: "Equo IDE",
      },
    ],
    ...updateDangerReviewComponent,
  },
]);
