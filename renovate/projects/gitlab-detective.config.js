const {
  createServerConfig,
  baseConfig,
  defaultLabels,
  availableRouletteReviewerByRole,
} = require("../lib/shared");
const { updateDangerReviewComponent } = require("../lib/components");

module.exports = createServerConfig([
  {
    repository: "gitlab-renovate-forks/gitlab-detective-spot",
    ...baseConfig,
    labels: [
      ...defaultLabels,
    ],
    reviewers: availableRouletteReviewerByRole("gitlab-detective-spot"),
    enabledManagers: ["bundler", "custom.regex"],
    postUpdateOptions: ["bundlerConservative"],
    semanticCommits: "disabled",
    ...updateDangerReviewComponent,
  },
]);